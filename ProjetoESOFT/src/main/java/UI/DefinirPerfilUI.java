
package UI;
import Controller.DefinirPerfilController;
import Model.Empresa;
import Utils.Utils;
/**
 *
 * @author Murilo Couceiro
 */
public class DefinirPerfilUI {
   private Empresa m_oEmpresa;
   private DefinirPerfilController m_controller; 
    
   
  public DefinirPerfilUI(Empresa oEmpresa){
      this.m_oEmpresa=oEmpresa;
      m_controller=new DefinirPerfilController(oEmpresa);
  }
  public void run(){
      System.out.println("\nNovo Perfil: ");
      m_controller.novoPerfil();
      
      introduzDados();
              
      apresentaDados();
  }
  
  private void introduzDados()
  {
      String descricao=Utils.readLineFromConsole("Introduza a descrição do perfil:");
      String Equipamentos=Utils.readLineFromConsole("Introduza os equipamentos autorizados para este perfil (separados por virgulas pf): ");
      
      m_controller.setDados(descricao, Equipamentos);
      m_controller.validarPerfil();
  }  
  
  private void apresentaDados(){
      System.out.println("\nPerfil:\n"+m_controller.PerfilToString());
  }
}
