
package UI;

import UI.EspecificarEquipamentoUI;
import java.io.IOException;
import Model.Empresa;
import Utils.Utils;

/**
 *
 * @author Murilo Couceiro
 */
public class MenuUI {
     private Empresa m_empresa;
    private String opcao;

    public MenuUI(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public void run() throws IOException
    {
        do
        {
            //opcao = "1";
            System.out.println("\n\n");
            System.out.println("1. Especificar Equipamento");
            System.out.println("2. Especificar Cartão");
            System.out.println("3. Definir Perfil");
            System.out.println("4. Registar Colaborador");
            System.out.println("5. Atribuir Cartão de Identificação");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if( opcao.equals("1") )
            {
                EspecificarEquipamentoUI ui = new EspecificarEquipamentoUI(m_empresa);
                ui.run();
            }

            // Incluir as restantes opções aqui
            
        }
        while (!opcao.equals("0") );
    }
}
