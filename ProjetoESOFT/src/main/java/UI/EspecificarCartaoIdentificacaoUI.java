package UI;

import Controller.EspecificarCartaoIdentificacaoController;
import Model.Empresa;
import Utils.Utils;
import java.util.Date;

/**
 *
 * @author Murilo Couceiro
 */
public class EspecificarCartaoIdentificacaoUI {

    private Empresa m_oEmpresa;
    private EspecificarCartaoIdentificacaoController m_controller;

    public EspecificarCartaoIdentificacaoUI(Empresa oEmpresa) {
        this.m_oEmpresa = oEmpresa;
        m_controller = new EspecificarCartaoIdentificacaoController(oEmpresa);
    }

    public void run() {
        System.out.println("\nNovo Equipamento: ");
        m_controller.novoCartao();
    
        introduzirDados();
        
        apresentaDados();
    }

    private void introduzirDados() {

        String versao = Utils.readLineFromConsole("Introduza a versão do cartão: ");
        Date dataEmissao = Utils.readDateFromConsole("Insira a data com o formato dd-mm-aa: ");

        m_controller.setDados(versao, dataEmissao);
        m_controller.validaCartao();
    }
    private void apresentaDados(){
        System.out.println("\nCartao:\n"+m_controller.getCartaoString());
}
    
}

