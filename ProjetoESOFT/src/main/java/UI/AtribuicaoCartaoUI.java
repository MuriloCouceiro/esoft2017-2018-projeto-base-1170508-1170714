package UI;

import Controller.AtribuicaoCartaoController;
import Model.Empresa;
import Utils.Utils;
import java.util.Date;


/**
 *
 * @author Murilo Couceiro
 */
public class AtribuicaoCartaoUI {
    private Empresa m_oEmpresa;
    private AtribuicaoCartaoController m_controller;

    public AtribuicaoCartaoUI(Empresa oEmpresa){
        this.m_oEmpresa=oEmpresa;
        m_controller=new AtribuicaoCartaoController(oEmpresa);
    }
    
    public void run(){
        System.out.println("\nNova Atribuição de Cartão: ");
        m_controller.novoAtribuicao();
    
        introduzirDados();
        
      
    
    }
    
    
    public void introduzirDados(){
        String datai=Utils.readLineFromConsole("Introduza a data do inicio");
        Date dataI=Utils.readDateFromConsole(datai);
        String dataf=Utils.readLineFromConsole("Introduza a data do fim");
        Date dataF=Utils.readDateFromConsole(dataf);
        String numMec=Utils.readLineFromConsole("Insira o numero Mecanografico do Colaborador");
  
       
    }



}
