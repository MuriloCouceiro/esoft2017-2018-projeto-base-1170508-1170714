/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.RegistarColaboradorController;
import Model.Empresa;
import Utils.Utils;
import Model.Perfil;

/**
 *
 * @author Murilo Couceiro
 */
public class RegistarColaboradorUI {
    private Empresa m_oEmpresa;
    private RegistarColaboradorController m_controller;
    
    public RegistarColaboradorUI(Empresa oEmpresa){
        this.m_oEmpresa=oEmpresa;
        m_controller=new RegistarColaboradorController(oEmpresa);
    }
    
    public void run()
    {
        
        System.out.println("\nNovo Colaborador:");
        m_controller.novoColaborador();
    
        introduzDados();
        
        apresentaDados();
    
    }
    
    private void introduzDados(){
        String numMec= Utils.readLineFromConsole("Introduzir numero mecanografico: ");
        String NomeComp=Utils.readLineFromConsole("Introduza o seu  nome completo: ");
        String nomeAbrev=Utils.readLineFromConsole("Introduza nome abreviado");
    
    Perfil p=(Perfil)Utils.apresentaESeleciona(m_oEmpresa.getListaPerfis(),"Escolha um perfil");
            
        m_controller.setDados(numMec, NomeComp, nomeAbrev, p);
    }
    
    private void apresentaDados(){
        System.out.println("\nColaborador"+m_controller.ColabradorToSTring());
    }
}
