/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Colaborador;
import Model.Empresa;
import Model.Perfil;

/**
 *
 * @author Murilo Couceiro
 */
public class RegistarColaboradorController {
    private Empresa m_oEmpresa;
    private Colaborador Colaborador;
    
    public RegistarColaboradorController(Empresa oEmpresa){
        this.m_oEmpresa=oEmpresa;
    }
    
    public void novoColaborador(){
        this.Colaborador=this.m_oEmpresa.novoColaborador();
    }
    public void setDados(String numMec, String nomeComp, String nomeAbrev, Perfil p){
      this.Colaborador.setNomeAbrev(nomeAbrev);
      this.Colaborador.setNomeComp(nomeComp);
      this.Colaborador.setNumMecan(numMec);
      this.Colaborador.setPerfil(p);
    }
   public String ColabradorToSTring(){
       return this.Colaborador.toString();
   }
}
