package Controller;

import java.util.Date;
import Model.AtribuicaoCartao;
import Model.Empresa;

/**
 *
 * @author Murilo Couceiro
 */
public class AtribuicaoCartaoController {

    private Empresa m_oEmpresa;
    private AtribuicaoCartao Atribuicao;

    public AtribuicaoCartaoController(Empresa oEmpresa) {
        this.m_oEmpresa = oEmpresa;
    }

    public void novoAtribuicao() {
        this.Atribuicao = this.m_oEmpresa.novoAtribuicao();
    }

    public void setDados(Date dataI, Date dataF, String numMec, int idCartao) {
        this.Atribuicao.setDataFim(dataF);
        this.Atribuicao.setDataInicio(dataI);
        this.Atribuicao.setNumMec(numMec);
        this.Atribuicao.setIdCartao(idCartao);

    }

    public void validarAtribuicao() {
        this.m_oEmpresa.validarAtribuicao(this.Atribuicao);
    }
    
    public boolean Atribuir(){
        return this.m_oEmpresa.Atribui(this.Atribuicao);
    }
    
    public String AtribuicaoToString(){
        return this.Atribuicao.toString();
    }
}
