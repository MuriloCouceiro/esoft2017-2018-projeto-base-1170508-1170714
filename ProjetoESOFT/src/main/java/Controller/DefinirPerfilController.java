package Controller;

import Model.Autorizacoes;
import Model.Empresa;
import Model.Equipamento;
import Model.Perfil;
import java.util.ArrayList;

/**
 *
 * @author Murilo Couceiro
 */
public class DefinirPerfilController {

    private Empresa m_oEmpresa;
    private Perfil Perfil;
    ArrayList<Autorizacoes> aut;

    public DefinirPerfilController(Empresa oEmpresa) {
        this.m_oEmpresa = oEmpresa;
    }

    
    public void novoPerfil(){
        this.Perfil=this.m_oEmpresa.novoPerfil();
    }

    
    public void setDados(String id,String descricao){
       this.Perfil.setId(id);
       this.Perfil.setDescricao(descricao);
    }

    public void validarPerfil(){
        this.m_oEmpresa.validaPerfil(this.Perfil);
    }
    
    public boolean definirPerfil(){
       return this.m_oEmpresa.definirPerfil(this.Perfil);
    }

    public String PerfilToString(){
        return this.Perfil.toString();
    }
    
    public Equipamento autorizaEquipamento(){
        return m_oEmpresa.getEquipamento("");
    }

    public boolean setPeridoUtilizacao(){
       return Perfil.Autorizacoes.addAll(aut);
    }
}

