/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.util.Date;
import Model.Empresa;
import Model.Cartao;

/**
 *
 * @author Murilo Couceiro
 */
public class EspecificarCartaoIdentificacaoController {

    private Empresa m_oEmpresa;
    private Cartao m_oCartao;

    public EspecificarCartaoIdentificacaoController(Empresa o_Empresa) {
        this.m_oEmpresa = o_Empresa;
    }

    public void novoCartao() {
        this.m_oCartao = this.m_oEmpresa.novoCartao();
    }

    public void setDados(String versao, Date dataEmissao) {
        this.m_oCartao.setID();
        this.m_oCartao.setDataEmissao(dataEmissao);
        this.m_oCartao.setVersao(versao);
    }

    public boolean registaCartao() {
        return this.m_oEmpresa.registaCartao(this.m_oCartao);
    }

    public String getCartaoString() {

        return this.m_oCartao.toString();
    }
    
    public void validaCartao(){
        this.m_oEmpresa.validaCartao(this.m_oCartao);
    } 
    
}
