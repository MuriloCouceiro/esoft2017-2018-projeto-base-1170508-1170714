/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.AcederAreaRestrita;
import Model.Empresa;


/**
 *
 * @author hjvp1
 */
public class AcederAreaRestritaController {
    
    private Empresa m_oEmpresa;
    private AcederAreaRestrita m_oAcesso;
    
    public AcederAreaRestritaController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    
    
    
    public void setDados(int id, String equip)
    {
        this.m_oAcesso.setIdCartao(id);
        this.m_oAcesso.setDataAcesso();
        this.m_oAcesso.setIdEquipamento(equip);
    }
    
   

    public String getAcessoString()
    {
        return this.m_oAcesso.toString();
    }

    
    public static void run(){
        
    }
    
}
