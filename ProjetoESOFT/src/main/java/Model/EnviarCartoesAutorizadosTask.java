/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Registos.RegistoCartoes;
import Registos.RegistoEquipamento;
import java.util.List;
import java.util.TimerTask;

/**
 *
 * @author Murilo Couceiro
 */
public abstract class EnviarCartoesAutorizadosTask extends TimerTask {

    private RegistoEquipamento re;
    private RegistoCartoes rc;
    private Empresa emp;

    public EnviarCartoesAutorizadosTask(RegistoEquipamento re, RegistoCartoes rc) {
        this.re = re;
        this.rc = rc;
    }

    public abstract void run();

    public boolean cancel() {
        return true;
    }

    public boolean enviarCartoesAutorizados() {
        return true;
    }

}
