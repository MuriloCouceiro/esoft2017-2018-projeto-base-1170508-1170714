/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Murilo Couceiro
 */
public class Colaborador {

    private String numMec;
    private String nomeComp;
    private String nomeAbrev;
    public Perfil perfil;

    public Colaborador() {

    }

    public Colaborador(String numMec, String nomeComp, String nomeAbrev, Perfil p) {
        this.setNumMecan(numMec);
        this.setNomeComp(nomeComp);
        this.setNumMecan(numMec);
        this.setPerfil(p);
    }

    public final void setNumMecan(String numMec) {
        this.numMec = numMec;
    }

    public final void setNomeComp(String nomeComp) {
        this.nomeComp = nomeComp;
    }

    public final void setNomeAbrev(String nomeAbrev) {
        this.nomeAbrev = nomeAbrev;

    }

    public String getNumMecan() {
        return this.numMec;
    }

    public final void setPerfil(Perfil p) {
        this.perfil = p;
    }

    public boolean valida(Colaborador c) {
        if (this.nomeComp.equalsIgnoreCase(c.nomeComp)|| this.numMec.equalsIgnoreCase(c.numMec)){
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
        return this.nomeAbrev+";"+this.nomeAbrev+";"+this.numMec+";"+this.perfil
                ;    
    }
    
}
