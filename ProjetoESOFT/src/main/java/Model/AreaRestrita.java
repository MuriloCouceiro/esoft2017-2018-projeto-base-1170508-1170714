/*
 * To change * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Murilo Couceiro
 */
public class AreaRestrita {
    
    private int codigo;
    private String designacao;
    private String localizacao;
    private int lotacaoMAx;
    
    public AreaRestrita(){
        
    }
    

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * @param designacao the designacao to set
     */
    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    /**
     * @return the localizacao
     */
    public String getLocalizacao() {
        return localizacao;
    }

    /**
     * @param localizacao the localizacao to set
     */
    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    /**
     * @return the lotacaoMAx
     */
    public int getLotacaoMAx() {
        return lotacaoMAx;
    }

    /**
     * @param lotacaoMAx the lotacaoMAx to set
     */
    public void setLotacaoMAx(int lotacaoMAx) {
        this.lotacaoMAx = lotacaoMAx;
    }
    public boolean valida(){
        //if true
       
        return true;
    }
}
