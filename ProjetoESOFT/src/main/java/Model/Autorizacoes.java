/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Murilo Couceiro
 */
public class Autorizacoes   implements Serializable {

    /**
     *
     */
    public Equipamento equipamento;

    /**
     *
     */
    public ArrayList<PeriodoUtilizacao> periodos;

    /**
     *
     */
    public Autorizacoes() {
        this.equipamento = new Equipamento();
        this.periodos = new ArrayList<>();
    }

    /**
     *
     * @param e
     * @param periodos
     */
    public Autorizacoes(Equipamento e, ArrayList<PeriodoUtilizacao> periodos) {

        this.setEquipamento(e);
        this.periodos = periodos;

    }

    /**
     *
     * @param e
     */
    public final void setEquipamento(Equipamento e) {
        this.equipamento = e;
    }

    /**
     *
     * @param per
     */
    public void setPeriodos(ArrayList per) {
        this.periodos = per;
    }

    /**
     * @return the equipamento
     */
    public Equipamento getEquipamento() {
        return equipamento;
    }

    /**
     *
     * @param p
     * @return
     */
    public boolean addPeriodo(PeriodoUtilizacao p) {
        return periodos.add(p);
    }

    /**
     *
     * @return
     */
    public ArrayList getPeriodos() {
        return this.periodos;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return equipamento.toString() + periodos.toString();
    }
    public boolean hasEquipamento(Equipamento e){
        return this.equipamento.equals(e);
    }
}
