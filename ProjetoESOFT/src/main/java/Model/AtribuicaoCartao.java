package Model;

import java.util.Date;
import Model.Colaborador;
/**
 *
 * @author Murilo Couceiro
 */
public class AtribuicaoCartao {
    private Date dataInicio;
    private Date dataFim;
    public String numMec;
    public int idCartao;
    
    public AtribuicaoCartao(){
        
    }
    public AtribuicaoCartao(Date dataI,Date dataF,String numMec,int idCartao){
        this.setDataInicio(dataI);
        this.setDataFim(dataF);
        this.setIdCartao(idCartao);
        this.setNumMec(numMec);
    }

    public void setDataInicio(Date dataI){
     this.dataInicio=dataI;
    }
    
    public void setDataFim(Date dataF){
      this.dataFim=dataF;  
    }
    
    public void setNumMec(String numMec){
        this.numMec=numMec;
    }
    public void setIdCartao(int id){
        this.idCartao=id;
    }
    
    public boolean valida(){
        if(dataInicio.after(dataFim) || idCartao<10000){
            return false;
        }
        return true;
    }
    
    public boolean valida(AtribuicaoCartao ac){
        if(this.numMec.equalsIgnoreCase(ac.numMec) || this.idCartao==ac.idCartao){
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
        return this.dataInicio+";"+this.dataFim+";"+this.numMec+";"+this.idCartao;
        
    }
}
