package Model;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import Model.AcederAreaRestrita;
import Model.AtribuicaoCartao;
import Model.Cartao;
import Model.Colaborador;
import Model.Equipamento;
import Model.Perfil;
import Registos.RegistoAcessos;
import Registos.RegistoAtribuicoes;
import Registos.RegistoCartoes;
import Registos.RegistoColaboradores;
import Registos.RegistoEquipamento;
import Registos.RegistoPerfis;
/**
 *
 * @author Murilo Couceiro
 */
public class Empresa {
   
    RegistoAcessos rA;
    RegistoAtribuicoes rAt;
    RegistoCartoes rc;
    RegistoColaboradores rCol;
    RegistoEquipamento rE;
    RegistoPerfis rP;
//    RegistoAreasRestritas rAR = new RegistoAreasRestritas();

//    private final ArrayList<Equipamento> Equipamentos;
//    public final ArrayList<Cartao> Cartoes;
//    public final ArrayList<Perfil> Perfis;
//    public final ArrayList<Colaborador> Colaboradores;
//    public final ArrayList<AtribuicaoCartao> AtribuicoesCartao;
//    private final List<AcederAreaRestrita> Acessos;
    /**
     *
     */
    public Empresa() {
        rE = new RegistoEquipamento();
        rCol = new RegistoColaboradores();
        rP = new RegistoPerfis();
        rc = new RegistoCartoes();
        rAt = new RegistoAtribuicoes();
        rA= new RegistoAcessos();
        



    }

    public RegistoAcessos getRegistoAcessos() {
        return rA;
    }
    public RegistoAtribuicoes getRegistoAtribuicoes() {

        return rAt;
    }

    public RegistoCartoes getRegistoCartoes() {
        return rc;
    }

    public RegistoColaboradores getRegistoColaboradores() {
        return rCol;
    }

    /**
     *
     * @return
     */
    public RegistoEquipamento getRegistoEquipamento() {
        return rE;
    }

    /**
     *
     * @return
     */
    public RegistoPerfis getRegistoPerfis() {
        return rP;
    }

    public Equipamento novoEquipamento()
    {
        return new Equipamento();
    }
    
    public boolean validaEquipamento(Equipamento e)
    {
        for (Equipamento equip: this.rE.getListaEquipamentos()) {
          if(!e.valida(equip)) {
             return false;
          } 
        }
         return true;
    }
    
    public boolean registaEquipamento(Equipamento e)
    {
        if (this.validaEquipamento(e))
        {
           return addEquipamento(e);
        }
        return false;
    }
    
    private boolean addEquipamento(Equipamento e)
    {
        return this.rE.registaEquipamento(e);
    }
    
    public List<Equipamento> getListaEquipamentos()
    {
        return this.rE.getListaEquipamentos();
    }

    public Equipamento getEquipamento(String sEquipamento)
    {
        for(Equipamento term : this.rE.getListaEquipamentos())
        {
            if (term.isIdentifiableAs(sEquipamento))
            {
                return term;
            }
        }
        
        return null;
    }
     public Cartao novoCartao()
    {
        return new Cartao();
    }
    
    public boolean validaCartao(Cartao cart)
    {
        for (Cartao c: this.rc.Cartoes) {
          if(c.valida(cart)) {
              return true;
          } 
        }
        return false;
    }
    
    public boolean registaCartao(Cartao c)
    {
        if (this.validaCartao(c))
        {
           return addCartao(c);
        }
        return false;
    }
    
    private boolean addCartao(Cartao c){
    
        return this.rc.Cartoes.add(c);
    }
    
    public Cartao getCartao(int idCartao)
    {
        for(Cartao term : this.rc.Cartoes)
        {
            if (term.isIdentifiableAs(idCartao))
            {
                return term;
            }
        }
        
        return null;
    }

    
    public Perfil novoPerfil()
    {
        return new Perfil();
    }
    
    public boolean validaPerfil(Perfil p)
    {
        for (Perfil pfs: this.rP.getListaPerfis()) {
          if(!pfs.isIdentifiableAs(p.getId())) {
             return false; 
          } 
        }
        return true;
    }
    
    public boolean definirPerfil(Perfil p)
    {
        if (this.validaPerfil(p))
        {
           return addPerfil(p);
        }
        return false;
    }
    
    private boolean addPerfil(Perfil p)
    {
        return this.rP.definirPerfil(p);
    }
    
    public List<Perfil> getListaPerfis()
    {
        return this.rP.getListaPerfis();
    }

    public Perfil getPerfil(String idperfil)
    {
        for(Perfil perfil : this.getListaPerfis())
       {
            if (perfil.isIdentifiableAs(idperfil))
            {
               return perfil;
           }
        }  
      return null;
    }
   
    public Colaborador novoColaborador()
    {
        return new Colaborador();
    }
    public ArrayList<Cartao> getListaCartoes()
    {
        return this.rc.Cartoes;
    }
    
   
      public boolean validaColaborador(Colaborador col)
    {
        for (Colaborador c: this.rCol.registoColaboradores) {
          if(!c.valida(col)) {
             return false; 
          } 
        }
        return true;
    }
    
      public AtribuicaoCartao novoAtribuicao(){
          return new AtribuicaoCartao();
      }
      
      public boolean validarAtribuicao(AtribuicaoCartao abc)
    {
        for (AtribuicaoCartao ac: this.rAt.atribuicoes) {
          if(!ac.valida(abc)) {
             return false; 
          } 
        }
        return true;
   }
      public boolean Atribui(AtribuicaoCartao ac)
    {
        if (this.validarAtribuicao(ac))
        {
           return addAtribuicao(ac);
        }
        return false;
    }
       private boolean addAtribuicao(AtribuicaoCartao ac)
    {
        return this.Atribui(ac);
    }
       
        public AcederAreaRestrita novoAcesso() {
        return new AcederAreaRestrita();
    }


    public boolean addAcesso(AcederAreaRestrita a) {
        return this.addAcesso(a);
    }

    public List<AcessoArea> getListaAcesso() {
        return this.rA.getAcessos();
    }
}

    //*public AtribuicaoCartao getAtribuicaoCartao(int idColaborador) {
//        for (Colaborador c : this.Colaboradores) {
//            if (c.isIdentifiableAs(idColaborador)) {
//                return c;
//            }
//        }
//
//        return null;
//    }
//      
//}   

      
    

    
