/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author Murilo Couceiro
 */
public class Cartao {

    private int num_cartoes = 0;
    private int ID;
    private String versao;
    private Date dataEmissao;
    public String categoria;

    public Cartao() {
        num_cartoes++;
    }

    public Cartao(String ID, String versao, Date dataEmissao,String categoria) {
        this.setID();
        this.setVersao(versao);
        this.setDataEmissao(dataEmissao);
        this.categoria=categoria;
        num_cartoes++;
    }

    public void setID() {
        this.ID = 10000 + num_cartoes;
    }

    public void setVersao(String versao) {
        this.versao = versao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public boolean valida() {
        if (this.getVersao().length() > 0) {
            return true;
        }

        return false;
    }

    public boolean valida(Cartao c) {
        if (this.ID == c.ID) {
            return false;
        }
        return true;
    }

    
    public int getID() {
        return ID;
    }

    
    public String getVersao() {
        return versao;
    }

    
    public Date getDataEmissao() {
        return dataEmissao;
    }

    public boolean isIdentifiableAs(int sID) {
        return this.ID == sID;
    }

    public boolean isIdentifiableAs(Date sID) {
        return this.dataEmissao.equals(sID);
    }

    @Override
    public String toString() {
        return ID + versao;
    }
}
