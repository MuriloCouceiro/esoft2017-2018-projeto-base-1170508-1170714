
package Model;

import Registos.RegistoEquipamento;
import java.util.Date;
import Model.Equipamento;
import Model.Equipamento;
import Model.Perfil;
import Model.Perfil;

public class AcessoArea {

    private int idCartao;
    private Date dataAcesso;
    private String idEquipamento;
    private int idColaborador;

    public AcessoArea() {
        
    }

    public AcessoArea(int idC, String idE) {
        this.setIdCartao(idC);
        this.setIdEquipamento(idE);
        this.setDataAcesso();
    }

    public final void setIdCartao(int n) {
        this.idCartao = n;
    }

    public int getIdCartao() {
        return this.idCartao;
    }

    public final void setIdEquipamento(String s) {
        this.idEquipamento = s;
    }
    
    public String getIdEquipamento() {
        return this.idEquipamento;
    }
    
    public final void setDataAcesso() {
        this.dataAcesso=new Date();
    }

    public Date getDataAcesso() {
        return this.dataAcesso;
    }


    public final void setIdColaborador(int n) {
        this.idColaborador = n;
    }

    public int getIdColaborador() {
        return this.idColaborador;
    }

    public boolean valida(Perfil p, RegistoEquipamento e) {
        Equipamento equip= e.getEquipamento(this.idEquipamento);
        
        if(p.Equipamentos.contains(equip)){
            return true;
        }
        else{
            return false;
        }
    }


    public boolean isIdentifiableAs(int sID) {
        return this.idCartao == sID;
    }

    @Override
    public String toString() {
        return this.idCartao + ";" + this.idColaborador;
    }


}
