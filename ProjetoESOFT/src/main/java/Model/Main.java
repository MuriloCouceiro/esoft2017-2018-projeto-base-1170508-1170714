/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import UI.MenuUI;
import java.io.IOException;

/**
 *
 * @author Murilo Couceiro
 */
public class Main {
     public static void main(String[] args)
    {
        try
        {
            Empresa empresa = new Empresa();

            MenuUI uiMenu = new MenuUI(empresa);

            uiMenu.run();
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
    }
}
