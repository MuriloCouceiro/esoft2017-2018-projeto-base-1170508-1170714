package Model;

import java.util.ArrayList;

/**
 *
 * @author Murilo Couceiro
 */
public class Equipamento {
    private String m_sDescricao;
    private String m_sEnderecoLogico;
    private String m_sEnderecoFisico;
    private String m_sFicheiroConfiguracao;
    
    public ArrayList<Cartao> cart;  
    
    public ArrayList<Autorizacoes> aut;
    
    public Equipamento()
    {
    }
    
    public Equipamento(String sDescricao,String sEnderecoLogico,String sEnderecoFisico,String sFicheiroConfiguracao)
    {
        this.setDescricao(sDescricao);
        this.setEnderecoLogico(sEnderecoLogico);
        this.setEnderecoFisico(sEnderecoFisico);
        this.setFicheiroConfiguracao(sFicheiroConfiguracao);
    }
    
    public final void setDescricao(String sDescricao)
    {
        this.m_sDescricao = sDescricao;
    }
    
    public final void setEnderecoLogico(String sEnderecoLogico)
    {
        this.m_sEnderecoLogico = sEnderecoLogico;
    }
    
    public String getEnderecoLogico()
    {
        return this.m_sEnderecoLogico;
    }
    
    public final void setEnderecoFisico(String sEnderecoFisico)
    {
        this.m_sEnderecoFisico = sEnderecoFisico;
    }
    
    public final void setFicheiroConfiguracao(String sFicheiroConfiguracao)
    {
        this.m_sFicheiroConfiguracao = sFicheiroConfiguracao;
    }
    
    public boolean valida(Equipamento e){
    
        if(this.m_sEnderecoFisico.equalsIgnoreCase(e.m_sEnderecoFisico) || this.m_sEnderecoLogico.equalsIgnoreCase(m_sEnderecoLogico)){
            return false;
        }
        
        return true;
    }
    
    public boolean valida(){
        if(this.m_sEnderecoLogico.split(".").length==4){
            return true;
            
        }
            return false;   
    }
    
    public boolean isIdentifiableAs(String sID)
    {
        return this.m_sEnderecoLogico.equals(sID);
    }
    
    @Override
    public String toString()
    {
        return this.m_sDescricao + ";" + this.m_sEnderecoLogico + ";...";
    }
}

