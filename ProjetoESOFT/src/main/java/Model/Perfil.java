package Model;

import java.util.ArrayList;

/**
 *
 * @author Murilo Couceiro
 */
public class Perfil {

   /**
     *
     * @return
     */
    public String getId() {
        return m_sId;
    }
    private int numId=0;
    private Empresa emp;
    private String m_sId;
    private String m_sDescricao;

    /**
     *
     */
    public ArrayList<Equipamento> Equipamentos;

    /**
     *
     */
    public ArrayList<Autorizacoes> Autorizacoes;
    
    /**
     *
     */
    public Perfil() {
        Equipamentos=new ArrayList<>();
        Autorizacoes=new ArrayList<>();
    }

    /**
     *
     * @param id
     * @param Descricao
     * @param Autorizacoes
     * @param equip
     * @param emp
     */
    public Perfil(String id, String Descricao,ArrayList<Autorizacoes> Autorizacoes,ArrayList<Equipamento> equip, Empresa emp) {
        this.setId(id);
        this.setDescricao(Descricao);
        this.setAutorizacoes(Autorizacoes);
        this.setEquipamentos(equip);    
        
    }
   
    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.m_sId = id;
    }

    /**
     *
     * @param descricao
     */
    public void setDescricao(String descricao) {
        this.m_sDescricao = descricao;
    }
    
    /**
     *
     * @param equip
     */
    public void setEquipamentos(ArrayList<Equipamento> equip){
        this.Equipamentos=equip;
    }
    
    /**
     *
     * @param aut
     */
    public void setAutorizacoes(ArrayList<Autorizacoes> aut){
        this.Autorizacoes=aut;
    }
    
    /**
     *
     * @param e
     */
    public final void addEquipamento(Equipamento e) {
    this.Equipamentos.add(e);
    }

    /**
     *
     * @return
     */
    public ArrayList<Equipamento> getEquipamentosAutorizados() {
        return this.Equipamentos;
    }

    public ArrayList<Autorizacoes> getAutorizacoes(){
        return this.Autorizacoes;
    }
    /**
     *
     * @return
     */
    @Override
    public String toString() {

     return "Perfil"+this.getId() + "\n" +
               "Descricao"+this.m_sDescricao + "\n" + 
               "Equipamentos"+this.Equipamentos.toString()+"\n"+
               "Autorizacoes"+this.Autorizacoes.toString()+"\n";

    }

    /**
     *
     * @param idPerfil
     * @return
     */
    public boolean isIdentifiableAs(String idPerfil) {
        return this.m_sId.equalsIgnoreCase(idPerfil);
    }
}