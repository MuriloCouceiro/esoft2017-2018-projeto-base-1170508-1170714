/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 *
 * @author Murilo Couceiro
 */
public class PeriodoUtilizacao  implements Serializable {

    /**
     *
     */
    public String dia_semana;

    /**
     *
     */
    public String hora_inicio;

    /**
     *
     */
    public String hora_fim;

    /**
     *
     * @param ds
     * @param hi
     * @param hf
     */
    public PeriodoUtilizacao(String ds, String hi, String hf) {
        this.setDiaSemana(ds);
        this.setHoraInicio(hi);
        this.setHoraFim(hf);
    }

    /**
     *
     * @param ds
     */
    public void setDiaSemana(String ds) {
        this.dia_semana = ds;
    }

    /**
     *
     * @param hi
     */
    public void setHoraInicio(String hi) {
        this.hora_inicio = hi;
    }

    /**
     *
     * @param hf
     */
    public void setHoraFim(String hf) {
        this.hora_fim = hf;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return this.dia_semana + "\n"+this.hora_inicio +"-"+ this.hora_fim;
    }

    public boolean pertence(String Dia, String horas) {
        String[] h = horas.split(":");
        String[] hF = this.hora_fim.split(":");
        String[] hI = this.hora_inicio.split(":");
        int hora = Integer.parseInt(h[0]);
        int min = Integer.parseInt(h[1]);

        int horaInicio = Integer.parseInt(hI[0]);
        int minInicio = Integer.parseInt(hI[1]);

        int horaFim = Integer.parseInt(hF[0]);
        int minFim = Integer.parseInt(hF[1]);

        if (Dia.equalsIgnoreCase(this.dia_semana)
                && ((hora < horaFim && hora > horaInicio)
                || (horaFim == hora && minFim > min)
                || (horaInicio == hora && min > minInicio))) {
            return true;
        }
        return false;
    }
}
