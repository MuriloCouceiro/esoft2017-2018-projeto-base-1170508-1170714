/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registos;

import java.util.ArrayList;
import java.util.List;
import Model.Equipamento;

/**
 *
 * @author Murilo Couceiro
 */
public class RegistoEquipamento {
    ArrayList<Equipamento> Equipamentos=new ArrayList<>();
    
        public static Equipamento novoEquipamento()
    {
        return new Equipamento();
    }
    
    public boolean validaEquipamento(Equipamento e)
    {
        for (Equipamento equip: this.Equipamentos) {
          if(!e.valida(equip)) {
             return false;
          } 
        }
         return true;
    }
    
    public boolean registaEquipamento(Equipamento e)
    {
        if (this.validaEquipamento(e))
        {
           return addEquipamento(e);
        }
        return false;
    }
    
    private boolean addEquipamento(Equipamento e)
    {
        return Equipamentos.add(e);
    }
    
    public List<Equipamento> getListaEquipamentos()
    {
        return this.Equipamentos;
    }

    public Equipamento getEquipamento(String sEquipamento)
    {
        for(Equipamento term : this.Equipamentos)
        {
            if (term.isIdentifiableAs(sEquipamento))
            {
                return term;
            }
        }
        
        return null;
    }
}
