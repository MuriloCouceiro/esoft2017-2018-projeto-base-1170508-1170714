/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registos;

import java.util.ArrayList;
import Model.Cartao;
import Model.Colaborador;

/**
 *
 * @author Murilo Couceiro
 */
public class RegistoColaboradores {

   public  ArrayList<Colaborador> registoColaboradores;
    private RegistoCartoes c;

    public Colaborador novoColaborador() {
        return new Colaborador();
    }

    public ArrayList<Cartao> getListaCartoes() {
        return c.Cartoes;
    }

    public boolean validaColaborador(Colaborador col) {
        for (Colaborador c : this.registoColaboradores) {
            if (!c.valida(col)) {
                return false;
            }
        }
        return true;

    }
    public Colaborador getColaboradorByID(String numMec){
        for (Colaborador reg: registoColaboradores) {
            if(reg.getNumMecan().equals(numMec))return reg; 
        }
        return null;
    }
}
