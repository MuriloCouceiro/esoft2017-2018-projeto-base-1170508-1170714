        /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registos;

import Model.AcessoArea;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import Model.AtribuicaoCartao;
import Model.Cartao;
import Model.Colaborador;

/**
 *
 * @author Murilo Couceiro
 */
public class RegistoAtribuicoes {
    

    public ArrayList<AtribuicaoCartao> atribuicoes;

     public void setDados(Date dataI,Date dataF,Cartao c,Colaborador col){
        
    }

    public AtribuicaoCartao novoAtribuicao() {
        return new AtribuicaoCartao();
    }

    public boolean validarAtribuicao(AtribuicaoCartao abc) {
        for (AtribuicaoCartao ac : this.atribuicoes) {
            if (!ac.valida(abc)) {
                return false;
            }
        }
        return true;
    }

    public boolean Atribui(AtribuicaoCartao ac) {
        if (this.validarAtribuicao(ac)) {
            return addAtribuicao(ac);
        }
        return false;
    }

    public AcessoArea novoAcesso() {
        return new AcessoArea();
    }

    private boolean addAtribuicao(AtribuicaoCartao ac) {
        return atribuicoes.add(ac);
    }

    
    public AtribuicaoCartao getAtribuicaoCartao(Cartao c){
        return new AtribuicaoCartao();
    }
    
    public boolean removerAtribuicao(AtribuicaoCartao ac){
       return this.atribuicoes.remove(ac);
    }
}
