/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registos;

import java.util.ArrayList;
import java.util.List;
import Model.Perfil;

/**
 *
 * @author Murilo Couceiro
 */
public class RegistoPerfis {
    ArrayList<Perfil> Perfis =new ArrayList<>();
    
      
    public Perfil novoPerfil()
    {
        return new Perfil();
    }
    
    public boolean validaPerfil(Perfil p)
    {
        for (Perfil pfs: this.Perfis) {
          if(!pfs.isIdentifiableAs(p.getId())) {
             return false; 
          } 
        }
        return true;
    }
    
    public boolean definirPerfil(Perfil p)
    {
        if (this.validaPerfil(p))
        {
           return addPerfil(p);
        }
        return false;
    }
    
    private boolean addPerfil(Perfil p)
    {
        return Perfis.add(p);
    }
    
    public List<Perfil> getListaPerfis()
    {
        return this.Perfis;
    }

    public Perfil getPerfil(String idperfil)
    {
        for(Perfil perfil : this.Perfis)
       {
            if (perfil.isIdentifiableAs(idperfil))
            {
               return perfil;
           }
        }  
      return null;
    }
   
  
}
