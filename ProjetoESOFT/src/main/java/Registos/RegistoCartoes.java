/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registos;

import java.util.ArrayList;
import Model.Cartao;

/**
 *
 * @author Murilo Couceiro
 */
public class RegistoCartoes {

    public ArrayList<Cartao> Cartoes;

    public Cartao novoCartao() {
        return new Cartao();
    }

    public boolean validaCartao(Cartao cart) {
        for (Cartao c : this.Cartoes) {
            if (c.valida(cart)) {
                return true;
            }
        }
        return false;
    }

    public boolean registaCartao(Cartao c) {
        if (this.validaCartao(c)) {
            return addCartao(c);
        }
        return false;
    }

    private boolean addCartao(Cartao c) {

        return Cartoes.add(c);
    }

    public Cartao getCartao(int idCartao) {
        for (Cartao term : this.Cartoes) {
            if (term.isIdentifiableAs(idCartao)) {
                return term;
            }
        }

        return null;
    }

    public boolean isProvisorio(Cartao c) {
        return c.categoria.equalsIgnoreCase("Provisorio");
    }

    public boolean isDefinitivo(Cartao c){
        return c.categoria.equalsIgnoreCase("Definitivo");
    }


}
